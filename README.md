## Implementation of the player's field of view through the DDA (Digital differential analyzer) algorithm

Based on this [video](https://www.youtube.com/watch?v=NbSee-XM7WA)

Blue is the walls and green is the floor

without:
![without](images/1.png)

with:
![with1](images/2.png)
![with2](images/3.png)
![with3](images/4.png)
