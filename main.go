package main

import (
	"fmt"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	W        = 800
	H        = 640
	TileSize = 16
	s        = 10
)

type Tile struct {
	x, y   float64
	isWall bool
	color  color.Color
	img    *ebiten.Image

	isVisible bool
}

type Player struct {
	x, y        float64
	img         *ebiten.Image
	speed       float64
	lineOfSight float64
}

func isCollide(x0, x1, y0, y1 float64) bool {
	return x0 < x1+TileSize &&
		x0+TileSize > x1 &&
		y0 < y1+TileSize &&
		y0+TileSize > y1
}

func (p *Player) Move(walls []*Tile) {
	var dx float64
	var dy float64

	if ebiten.IsKeyPressed(ebiten.KeyW) {
		dy -= p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyS) {
		dy += p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyA) {
		dx -= p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyD) {
		dx += p.speed
	}

	p.x += dx
	for _, obs := range walls {
		if isCollide(p.x, obs.x, p.y, obs.y) {
			if dx > 0 {
				p.x = obs.x - TileSize
			}
			if dx < 0 {
				p.x = obs.x + TileSize
			}
		}
	}

	p.y += dy
	for _, obs := range walls {
		if isCollide(p.x, obs.x, p.y, obs.y) {
			if dy > 0 {
				p.y = obs.y - TileSize
			}
			if dy < 0 {
				p.y = obs.y + TileSize
			}
		}
	}

}

type Game struct {
	tiles [][]*Tile
	// tiles []*Tile
	op    *ebiten.DrawImageOptions
	walls []*Tile

	p *Player
}

func (g *Game) createTiles() {
	g.tiles = make([][]*Tile, 0)
	for y := 0; y < H/TileSize; y++ {
		temp := make([]*Tile, 0)
		for x := 0; x < W/TileSize; x++ {
			tile := &Tile{
				x:         float64(x) * TileSize,
				y:         float64(y) * TileSize,
				color:     color.RGBA{0, 100, 100, 255},
				img:       ebiten.NewImageFromImage(ebiten.NewImage(TileSize, TileSize)),
				isVisible: true,
			}
			if x == 0 || y == 0 ||
				x == W/TileSize-1 || y == H/TileSize-1 ||
				(x == 30 && y > 10) ||
				(x == 10 && y < 30) {
				tile.isWall = true
				tile.color = color.RGBA{0, 0, 255, 255}
				tile.img.Fill(tile.color)
				g.walls = append(g.walls, tile)
			}
			tile.img.Fill(tile.color)
			temp = append(temp, tile)
		}
		g.tiles = append(g.tiles, temp)

	}
}

func (g *Game) Update() error {
	g.p.Move(g.walls)

	for _, row := range g.tiles {
		for _, t := range row {
			t.isVisible = false
		}
	}

	for y := g.p.y - g.p.lineOfSight; y <= g.p.y+g.p.lineOfSight; y++ {
		for x := g.p.x - g.p.lineOfSight; x <= g.p.x+g.p.lineOfSight; x++ {
			xDelta := g.p.x - x
			yDelta := g.p.y - y
			d := math.Sqrt(float64(xDelta*xDelta + yDelta*yDelta))
			if d <= float64(g.p.lineOfSight) && d >= g.p.lineOfSight-TileSize {
				g.dda(int(x), int(y))
			}

		}
	}

	fmt.Println(ebiten.ActualTPS())

	return nil
}

func (g *Game) dda(cx, cy int) {

	// player pos
	startX := g.p.x
	startY := g.p.y

	// direction
	dirX := float64(cx) - startX
	dirY := float64(cy) - startY
	mag := 1 / math.Sqrt(dirX*dirX+dirY*dirY)

	// normalized direction
	dirXN := dirX * mag
	dirYN := dirY * mag

	vRayUnitStepSizeX := math.Sqrt(1 + (dirYN/dirXN)*(dirYN/dirXN))
	vRayUnitStepSizeY := math.Sqrt(1 + (dirXN/dirYN)*(dirXN/dirYN))

	vMapCheckX := int(math.Floor(float64(startX)/TileSize)) * TileSize
	vMapCheckY := int(math.Floor(float64(startY)/TileSize)) * TileSize

	var vRayLen1DX, vRayLen1DY float64
	var vStepX, vStepY int

	if dirXN < 0 {
		vStepX = -1
		vRayLen1DX = (startX - float64(vMapCheckX)) * vRayUnitStepSizeX
	} else {
		vStepX = 1
		vRayLen1DX = (float64(vMapCheckX+1) - startX) * vRayUnitStepSizeX
	}
	if dirYN < 0 {
		vStepY = -1
		vRayLen1DY = (startY - float64(vMapCheckY)) * vRayUnitStepSizeY
	} else {
		vStepY = 1
		vRayLen1DY = (float64(vMapCheckY+1) - startY) * vRayUnitStepSizeY
	}

	tileFound := false
	maxDistanceX, maxDistanceY := g.p.lineOfSight, g.p.lineOfSight
	distanceX, distanceY := 0.0, 0.0
	for !tileFound && distanceX < maxDistanceX && distanceY < maxDistanceY {
		// walk
		if vRayLen1DX < vRayLen1DY {
			vMapCheckX += vStepX
			distanceX = vRayLen1DX
			vRayLen1DX += vRayUnitStepSizeX
		} else {
			vMapCheckY += vStepY
			distanceY = vRayLen1DY
			vRayLen1DY += vRayUnitStepSizeY
		}

		if vMapCheckX >= 0 && vMapCheckX < W &&
			vMapCheckY >= 0 && vMapCheckY < H {
			t := g.GetTile(vMapCheckX, vMapCheckY)
			t.isVisible = true

			if t.isWall {
				tileFound = true
			}

		}
	}

}

func (g *Game) GetTile(mx, my int) *Tile {
	tx := int(math.Floor(float64(mx) / TileSize))
	ty := int(math.Floor(float64(my) / TileSize))
	return g.tiles[ty][tx]
}

func (g *Game) Draw(screen *ebiten.Image) {

	for _, row := range g.tiles {
		for _, tile := range row {
			g.op.GeoM.Reset()
			g.op.GeoM.Translate(float64(tile.x), float64(tile.y))

			if tile.isVisible {
				screen.DrawImage(tile.img, g.op)
			}

		}
	}

	g.op.GeoM.Reset()
	g.op.GeoM.Translate(float64(g.p.x), float64(g.p.y))
	screen.DrawImage(g.p.img, g.op)

}

func (g *Game) Layout(w, h int) (int, int) {
	return W, H
}

func main() {
	g := &Game{
		op: &ebiten.DrawImageOptions{},
	}

	p := &Player{
		x:           TileSize * 24,
		y:           TileSize * 20,
		speed:       5,
		img:         ebiten.NewImageFromImage(ebiten.NewImage(TileSize, TileSize)),
		lineOfSight: TileSize * 4,
	}
	p.img.Fill(color.RGBA{255, 0, 0, 255})
	g.p = p

	g.createTiles()

	ebiten.SetWindowSize(W, H)

	if err := ebiten.RunGame(g); err != nil {
		panic(err)
	}
}
